package com.demo.service.servicedemo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.service.servicedemo.pojo.Student;

@RestController
@RequestMapping("/service-demo")
public class ServiceDemoController {

	@GetMapping("/basic-api")	
	public String basicApi() {
		return "Application running!";
	}
	
	@GetMapping("/get/student")
	public Student getAnyStudent() {
		
		List<Student> studentsList = new ArrayList<>();
		studentsList.add(new Student(12,"Suchait","CSE"));
		studentsList.add(new Student(13,"Suchait13","CSE"));
		studentsList.add(new Student(14,"Suchait14","CSE"));
		studentsList.add(new Student(15,"Suchait15","CSE"));
		studentsList.add(new Student(16,"Suchait16","CSE"));
		
		Random random = new Random();
		Integer number = random.nextInt(4);
		
		return studentsList.get(number);
	}
}
